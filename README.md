### 기자재 관리 API
***
(개인프로젝트) 기자재를 관리하는 API
***

### Language
```
JAVA 16
SpringBoot 2.7.3
```

### 기능
``` 
* 사원 관리
 - 사원 정보 등록
 - 사원 상세 정보 가져오기
 - 사원 정보 리스트 가져오기
 - 사원 직급 수정하기
 - 사원 정보 삭제하기

* 기자재 관리
 - 기자재 정보 등록하기
 - 기자재 상세 정보 가져오기
 - 기가재 정보 리스트 가져오기
 - 기자재 정보 삭제하기
 
* 기자재 사용내역 관리
 - 기자재 사용내역 등록하기
 - 기간에 해당되는 기자재 사용내역 리스트 가져오기
```

### 예시
>* 스웨거 전체 화면
>
>![swagger_all](./images/swagger-all.png)

> * 스웨거 사원 관리 화면
>
>![swagger_member](./images/swagger-member.png)

>* 스웨거 기자재 관리 화면
>
>![swagger_equipment](./images/swagger-equipment.png)

>* 스웨거 기자재 사용내역 관리 화면
>
>![swagger_usageHistory](./images/swagger-usagehistory.png)