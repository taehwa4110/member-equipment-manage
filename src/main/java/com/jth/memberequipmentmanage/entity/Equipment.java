package com.jth.memberequipmentmanage.entity;

import com.jth.memberequipmentmanage.enums.Classification;
import com.jth.memberequipmentmanage.interfaces.CommonModelBuilder;
import com.jth.memberequipmentmanage.model.EquipmentBuyRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Equipment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 15)
    @Enumerated(value = EnumType.STRING)
    private Classification classification;

    @Column(nullable = false, length = 20)
    private String name;

    @Column(nullable = false, length = 20)
    private String brand;

    @Column(nullable = false)
    private Double price;

    @Column(nullable = false)
    private Boolean isUsed;

    @Column(nullable = false)
    private LocalDateTime datePurchase;

    private LocalDateTime dateDisposal;

    public void putDisposal() {
        this.isUsed = false;
        this.dateDisposal = LocalDateTime.now();
    }

    private Equipment(EquipmentBuilder builder) {
        this.classification = builder.classification;
        this.name = builder.name;
        this.brand = builder.brand;
        this.price = builder.price;
        this.isUsed = builder.isUsed;
        this.datePurchase = builder.datePurchase;
    }

    public static class EquipmentBuilder implements CommonModelBuilder<Equipment> {
        private final Classification classification;
        private final String name;
        private final String brand;
        private final Double price;
        private final Boolean isUsed;
        private final LocalDateTime datePurchase;

        public EquipmentBuilder(EquipmentBuyRequest request) {
            this.classification = request.getClassification();
            this.name = request.getName();
            this.brand = request.getBrand();
            this.price = request.getPrice();
            this.isUsed = true;
            this.datePurchase = LocalDateTime.now();
        }

        @Override
        public Equipment build() {
            return new Equipment(this);
        }
    }
}
