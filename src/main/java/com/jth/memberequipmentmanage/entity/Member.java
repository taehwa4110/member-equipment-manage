package com.jth.memberequipmentmanage.entity;

import com.jth.memberequipmentmanage.enums.Gender;
import com.jth.memberequipmentmanage.interfaces.CommonModelBuilder;
import com.jth.memberequipmentmanage.model.MemberJoinRequest;
import com.jth.memberequipmentmanage.model.MemberRankUpdateRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Member {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 20)
    private String name;

    @Column(nullable = false, length = 10)
    private String rank;

    @Column(nullable = false)
    private LocalDate birthday;

    @Column(nullable = false, length = 20)
    private String phone;

    @Column(nullable = false, length = 10)
    @Enumerated(value = EnumType.STRING)
    private Gender gender;

    @Column(nullable = false)
    private Boolean isEnable;

    @Column(nullable = false)
    private LocalDateTime dateJoin;

    private LocalDateTime dateLeave;

    @Column(nullable = false)
    private LocalDateTime dateCreate;

    @Column(nullable = false)
    private LocalDateTime dateUpdate;

    public void putRank(MemberRankUpdateRequest request) {
        this.rank = request.getRank();
        this.dateUpdate = LocalDateTime.now();
    }

    public void putLeave() {
        this.isEnable = false;
        this.dateLeave = LocalDateTime.now();
    }

    private Member(MemberBuilder builder) {
        this.name = builder.name;
        this.rank = builder.rank;
        this.birthday = builder.birthday;
        this.phone = builder.phone;
        this.gender = builder.gender;
        this.isEnable = builder.isEnable;
        this.dateJoin = builder.dateJoin;
        this.dateCreate = builder.dateCreate;
        this.dateUpdate = builder.dateUpdate;
    }



    public static class MemberBuilder implements CommonModelBuilder<Member> {
        private final String name;
        private final String rank;
        private final LocalDate birthday;
        private final String phone;
        private final Gender gender;
        private final Boolean isEnable;
        private final LocalDateTime dateJoin;
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;

        public MemberBuilder(MemberJoinRequest request) {
            this.name = request.getName();
            this.rank = request.getRank();
            this.birthday = request.getBirthday();
            this.phone = request.getPhone();
            this.gender = request.getGender();
            this.isEnable = true;
            this.dateJoin = LocalDateTime.now();
            this.dateCreate = LocalDateTime.now();
            this.dateUpdate = LocalDateTime.now();
        }

        @Override
        public Member build() {
            return new Member(this);
        }
    }
}
