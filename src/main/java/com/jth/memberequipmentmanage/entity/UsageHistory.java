package com.jth.memberequipmentmanage.entity;

import com.jth.memberequipmentmanage.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class UsageHistory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "memberId", nullable = false)
    private Member member;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "equipmentId", nullable = false)
    private Equipment equipment;

    @Column(nullable = false)
    private LocalDateTime dateUsage;

    private UsageHistory(UsageHistoryBuilder builder) {
        this.member = builder.member;
        this.equipment = builder.equipment;
        this.dateUsage = builder.dateUsage;
    }

    public static class UsageHistoryBuilder implements CommonModelBuilder<UsageHistory> {
        private final Member member;
        private final Equipment equipment;
        private final LocalDateTime dateUsage;

        public UsageHistoryBuilder(Member member, Equipment equipment, LocalDateTime dateUsage) {
            this.member = member;
            this.equipment = equipment;
            this.dateUsage = dateUsage;
        }

        @Override
        public UsageHistory build() {
            return new UsageHistory(this);
        }
    }
}
