package com.jth.memberequipmentmanage.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
