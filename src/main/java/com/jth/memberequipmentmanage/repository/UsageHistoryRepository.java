package com.jth.memberequipmentmanage.repository;

import com.jth.memberequipmentmanage.entity.UsageHistory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.List;

public interface UsageHistoryRepository extends JpaRepository<UsageHistory, Long> {
    List<UsageHistory> findAllByDateUsageGreaterThanEqualAndDateUsageLessThanEqualOrderByIdDesc(LocalDateTime dateStart, LocalDateTime dateEnd);
}
