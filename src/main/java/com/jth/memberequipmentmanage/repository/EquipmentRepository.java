package com.jth.memberequipmentmanage.repository;

import com.jth.memberequipmentmanage.entity.Equipment;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EquipmentRepository extends JpaRepository<Equipment, Long> {
    List<Equipment> findAllByIsUsedOrderByIdDesc(Boolean isUsed);
}
