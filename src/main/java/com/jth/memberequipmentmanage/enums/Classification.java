package com.jth.memberequipmentmanage.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Classification {
    MACHINE("기계"),
    TOOL("기구"),
    MATERIAL("자재");

    private final String name;
}
