package com.jth.memberequipmentmanage.service;

import com.jth.memberequipmentmanage.entity.Member;
import com.jth.memberequipmentmanage.exception.CMissingDataException;
import com.jth.memberequipmentmanage.exception.CNoMemberDataException;
import com.jth.memberequipmentmanage.model.*;
import com.jth.memberequipmentmanage.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MemberService {
    private final MemberRepository memberRepository;

    /**
     * 사원 등록하기
     * @param request 사원 등록을 하기 위해 필요로 하는 값
     */
    public void setMember(MemberJoinRequest request) {
        Member member = new Member.MemberBuilder(request).build();
        memberRepository.save(member);
    }

    /**
     * 사원 정보 주기
     * @param id 사원 시퀀스
     * @return 사원 정보
     */
    public Member getMemberData(long id) {
        return memberRepository.findById(id).orElseThrow(CMissingDataException::new);
    }

    /**
     * 사원 상세정보 가져오기
     * @param id 사원 시퀀스
     * @return 사원 상세정보
     */
    public MemberDetail getMember(long id) {
        Member member = memberRepository.findById(id).orElseThrow(CMissingDataException::new);
        return new MemberDetail.MemberDetailBuilder(member).build();
    }

    /**
     * 사원 리스트 가져오기
     * @return 사원 리스트
     */
    public ListResult<MemberItem> getMembers() {
        List<MemberItem> result = new LinkedList<>();

        List<Member> members = memberRepository.findAll();

        members.forEach(member -> {
            MemberItem memberItem = new MemberItem.MemberItemBuilder(member).build();
            result.add(memberItem);
        });

        return ListConvertService.settingResult(result);
    }

    /**
     * 근속 여부에 따른 사원 리스트 가져오기
     * @param isEnable 사원 근속 여부
     * @return 근속 여부에 따른 사원 리스트
     */
    public ListResult<MemberItem> getMembers(Boolean isEnable) {
        List<Member> members = memberRepository.findAllByIsEnableOrderByIdDesc(isEnable);

        List<MemberItem> result = new LinkedList<>();

        members.forEach(member -> {
            MemberItem memberItem = new MemberItem.MemberItemBuilder(member).build();
            result.add(memberItem);
        });

        return ListConvertService.settingResult(result);
    }

    /**
     * 사원 직급 수정하기
     * @param id 사원 시퀀스
     * @param request 사원 직급을 수정하기 위해 필요로 하는 값
     */
    public void putMemberRank(long id, MemberRankUpdateRequest request) {
        Member member = memberRepository.findById(id).orElseThrow(CMissingDataException::new);
        member.putRank(request);

        memberRepository.save(member);
    }

    /**
     * 사원 정보 삭제하기
     * @param id 사원 시퀀스
     */
    public void putMemberLeave(long id) {
        Member member = memberRepository.findById(id).orElseThrow(CMissingDataException::new);

        if (!member.getIsEnable())throw new CNoMemberDataException();

        member.putLeave();
        memberRepository.save(member);
    }
}
