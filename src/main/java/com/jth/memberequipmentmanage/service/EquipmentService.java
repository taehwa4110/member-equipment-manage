package com.jth.memberequipmentmanage.service;

import com.jth.memberequipmentmanage.entity.Equipment;
import com.jth.memberequipmentmanage.exception.CMissingDataException;
import com.jth.memberequipmentmanage.exception.CNoMemberDataException;
import com.jth.memberequipmentmanage.model.EquipmentBuyRequest;
import com.jth.memberequipmentmanage.model.EquipmentDetail;
import com.jth.memberequipmentmanage.model.EquipmentItem;
import com.jth.memberequipmentmanage.model.ListResult;
import com.jth.memberequipmentmanage.repository.EquipmentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class EquipmentService {
    private final EquipmentRepository equipmentRepository;

    /**
     * 기자재 등록하기
     * @param request 기자재를 등록하기 위해 필요로 하는 값
     */
    public void setEquipment(EquipmentBuyRequest request) {
        Equipment equipment = new Equipment.EquipmentBuilder(request).build();
        equipmentRepository.save(equipment);
    }

    /**
     * 기자재 정보 주기
     * @param id 기자재 시퀀스
     * @return 기자재 정보
     */
    public Equipment getEquipmentData(long id) {
        return equipmentRepository.findById(id).orElseThrow(CMissingDataException::new);
    }

    /**
     * 기자재 상세정보 가져오기
     * @param id 기자재 시퀀스
     * @return 기자재 상세정보
     */
    public EquipmentDetail getEquipment(long id) {
        Equipment equipment = equipmentRepository.findById(id).orElseThrow(CMissingDataException::new);
        return new EquipmentDetail.EquipmentDetailBuilder(equipment).build();
    }

    /**
     * 기자재 리스트 가져오기
     * @return 기자재 리스트
     */
    public ListResult<EquipmentItem> getEquipments() {
        List<EquipmentItem> result = new LinkedList<>();

        List<Equipment> equipments = equipmentRepository.findAll();

        equipments.forEach(equipment -> {
            EquipmentItem equipmentItem = new EquipmentItem.EquipmentItemBuilder(equipment).build();
            result.add(equipmentItem);
        });

        return ListConvertService.settingResult(result);
    }

    /**
     * 사용여부에 따른 기자재 리스트 가져오기
     * @param isUsed 사용 여부
     * @return 사용여부에 따른 기자재 리스트
     */
    public ListResult<EquipmentItem> getEquipments(Boolean isUsed) {
        List<Equipment> equipments = equipmentRepository.findAllByIsUsedOrderByIdDesc(isUsed);

        List<EquipmentItem> result = new LinkedList<>();

        equipments.forEach(equipment -> {
            EquipmentItem equipmentItem = new EquipmentItem.EquipmentItemBuilder(equipment).build();
            result.add(equipmentItem);
        });

        return ListConvertService.settingResult(result);
    }

    /**
     * 기자재 정보 삭제하기
     * @param id 기자재 시퀀스
     */
    public void putEquipmentDisposal(long id) {
        Equipment equipment = equipmentRepository.findById(id).orElseThrow(CMissingDataException::new);

        if (!equipment.getIsUsed())throw new CNoMemberDataException();

        equipment.putDisposal();
        equipmentRepository.save(equipment);
    }
}
