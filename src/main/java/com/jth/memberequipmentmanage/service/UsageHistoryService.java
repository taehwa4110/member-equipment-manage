package com.jth.memberequipmentmanage.service;

import com.jth.memberequipmentmanage.entity.Equipment;
import com.jth.memberequipmentmanage.entity.Member;
import com.jth.memberequipmentmanage.entity.UsageHistory;
import com.jth.memberequipmentmanage.model.ListResult;
import com.jth.memberequipmentmanage.model.UsageHistoryItem;
import com.jth.memberequipmentmanage.repository.UsageHistoryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class UsageHistoryService {
    private final UsageHistoryRepository usageHistoryRepository;


    /**
     * 기자재 사용내역 등록하기
     * @param member 사원 정보
     * @param equipment 기자재 정보
     * @param dateUsage 사용 날짜
     */
    public void setUsageHistory(Member member, Equipment equipment, LocalDateTime dateUsage) {
        UsageHistory usageHistory = new UsageHistory.UsageHistoryBuilder(member, equipment, dateUsage).build();
        usageHistoryRepository.save(usageHistory);
    }

    /**
     * 기간에 해당되는 기자재 사용내역 리스트 가져오기
     * @param dateStart 시작일
     * @param dateEnd 종료일
     * @return 기간에 해당되는 기자재 사용내역 리스트
     */
    public ListResult<UsageHistoryItem> getUsageHistories(LocalDate dateStart, LocalDate dateEnd) {
        LocalDateTime dateStartTime = LocalDateTime.of(dateStart.getYear(),
                dateStart.getMonthValue(),
                dateStart.getDayOfMonth(),
                0,
                0,
                0
        );
        LocalDateTime dateEndTime = LocalDateTime.of(dateEnd.getYear(),
                dateEnd.getMonthValue(),
                dateEnd.getDayOfMonth(),
                23,
                59,
                59
        );
        List<UsageHistory> usageHistories = usageHistoryRepository.findAllByDateUsageGreaterThanEqualAndDateUsageLessThanEqualOrderByIdDesc(dateStartTime, dateEndTime);

        List<UsageHistoryItem> result = new LinkedList<>();

        usageHistories.forEach(usageHistory -> {
            UsageHistoryItem usageHistoryItem = new UsageHistoryItem.UsageHistoryItemBuilder(usageHistory).build();
            result.add(usageHistoryItem);
        });

        return ListConvertService.settingResult(result);
    }
}
