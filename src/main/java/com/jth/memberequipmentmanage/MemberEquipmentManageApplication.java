package com.jth.memberequipmentmanage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MemberEquipmentManageApplication {

	public static void main(String[] args) {
		SpringApplication.run(MemberEquipmentManageApplication.class, args);
	}

}
