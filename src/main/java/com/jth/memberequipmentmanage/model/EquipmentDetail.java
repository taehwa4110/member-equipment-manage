package com.jth.memberequipmentmanage.model;

import com.jth.memberequipmentmanage.entity.Equipment;
import com.jth.memberequipmentmanage.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class EquipmentDetail {
    @ApiModelProperty(notes = "기자재 시퀀스")
    private Long id;
    @ApiModelProperty(notes = "기자재 분류")
    private String classification;
    @ApiModelProperty(notes = "기자재명")
    private String name;
    @ApiModelProperty(notes = "기자재 브랜드")
    private String brand;
    @ApiModelProperty(notes = "기자재 가격")
    private String price;
    @ApiModelProperty(notes = "기자재 사용 가능 여부")
    private String isUsed;
    @ApiModelProperty(notes = "기자재 구입일")
    private LocalDateTime datePurchase;
    @ApiModelProperty(notes = "기자재 폐기일")
    private LocalDateTime dateDisposal;

    private EquipmentDetail(EquipmentDetailBuilder builder) {
        this.id = builder.id;
        this.classification = builder.classification;
        this.name = builder.name;
        this.brand = builder.brand;
        this.price = builder.price;
        this.isUsed = builder.isUsed;
        this.datePurchase = builder.datePurchase;
        this.dateDisposal = builder.dateDisposal;
    }

    public static class EquipmentDetailBuilder implements CommonModelBuilder<EquipmentDetail> {
        private final Long id;
        private final String classification;
        private final String name;
        private final String brand;
        private final String price;
        private final String isUsed;
        private final LocalDateTime datePurchase;
        private final LocalDateTime dateDisposal;

        public EquipmentDetailBuilder(Equipment equipment) {
            this.id = equipment.getId();
            this.classification = equipment.getClassification().getName();
            this.name = equipment.getName();
            this.brand = equipment.getBrand();
            this.price = equipment.getPrice() + "원";
            this.isUsed = equipment.getIsUsed() ? "예" : "아니오";
            this.datePurchase = equipment.getDatePurchase();
            this.dateDisposal = equipment.getDateDisposal();
        }

        @Override
        public EquipmentDetail build() {
            return new EquipmentDetail(this);
        }
    }
}
