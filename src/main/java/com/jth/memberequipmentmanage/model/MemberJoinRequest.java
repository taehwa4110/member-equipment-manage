package com.jth.memberequipmentmanage.model;

import com.jth.memberequipmentmanage.enums.Gender;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class MemberJoinRequest {
    @ApiModelProperty(notes = "사원 이름 (2~20자)", required = true)
    @NotNull
    @Length(min = 2, max = 20)
    private String name;

    @ApiModelProperty(notes = "사원 직급 (2~10자)", required = true)
    @NotNull
    @Length(min = 2, max = 10)
    private String rank;

    @ApiModelProperty(notes = "사원 생년월일", required = true)
    @NotNull
    private LocalDate birthday;

    @ApiModelProperty(notes = "사원 전화번호 (2~20자)", required = true)
    @NotNull
    @Length(min = 2, max = 20)
    private String phone;

    @ApiModelProperty(notes = "사원 성별", required = true)
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private Gender gender;
}
