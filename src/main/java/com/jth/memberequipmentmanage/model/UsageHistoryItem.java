package com.jth.memberequipmentmanage.model;

import com.jth.memberequipmentmanage.entity.UsageHistory;
import com.jth.memberequipmentmanage.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class UsageHistoryItem {
    @ApiModelProperty(notes = "사용내역 시퀀스")
    private Long usageHistoryId;
    @ApiModelProperty(notes = "사원명과 사원 계급")
    private String memberFullName;
    @ApiModelProperty(notes = "사원 전화번호")
    private String memberPhone;
    @ApiModelProperty(notes = "기자재 분류와 기자재명")
    private String equipmentFullName;
    @ApiModelProperty(notes = "기자재 사용 날짜")
    private LocalDateTime dateUsage;

    private UsageHistoryItem(UsageHistoryItemBuilder builder) {
        this.usageHistoryId = builder.usageHistoryId;
        this.memberFullName = builder.memberFullName;
        this.memberPhone = builder.memberPhone;
        this.equipmentFullName = builder.equipmentFullName;
        this.dateUsage = builder.dateUsage;
    }

    public static class UsageHistoryItemBuilder implements CommonModelBuilder<UsageHistoryItem> {
        private final Long usageHistoryId;
        private final String memberFullName;
        private final String memberPhone;
        private final String equipmentFullName;
        private final LocalDateTime dateUsage;

        public UsageHistoryItemBuilder(UsageHistory usageHistory) {
            this.usageHistoryId = usageHistory.getId();
            this.memberFullName = usageHistory.getMember().getName() + usageHistory.getMember().getRank();
            this.memberPhone = usageHistory.getMember().getPhone();
            this.equipmentFullName = "[" + usageHistory.getEquipment().getClassification().getName() + "]" + usageHistory.getEquipment().getName();
            this.dateUsage = usageHistory.getDateUsage();
        }

        @Override
        public UsageHistoryItem build() {
            return new UsageHistoryItem(this);
        }
    }
}
