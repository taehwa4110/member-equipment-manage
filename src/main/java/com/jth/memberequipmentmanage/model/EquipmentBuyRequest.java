package com.jth.memberequipmentmanage.model;

import com.jth.memberequipmentmanage.enums.Classification;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class EquipmentBuyRequest {
    @ApiModelProperty(notes = "기자재 분류", required = true)
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private Classification classification;

    @ApiModelProperty(notes = "기자재명 (2~20자)", required = true)
    @NotNull
    @Length(min = 2, max = 20)
    private String name;

    @ApiModelProperty(notes = "기자재 브랜드 (2~20자)", required = true )
    @NotNull
    @Length(min = 2, max = 20)
    private String brand;

    @ApiModelProperty(notes = "기자재 가격", required = true)
    @NotNull
    private Double price;
}
