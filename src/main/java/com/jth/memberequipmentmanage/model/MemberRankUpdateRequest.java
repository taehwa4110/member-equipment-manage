package com.jth.memberequipmentmanage.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class MemberRankUpdateRequest {
    @ApiModelProperty(notes = "사원 직급 (2~10자)", required = true)
    @NotNull
    @Length(min = 2, max = 10)
    private String rank;
}
