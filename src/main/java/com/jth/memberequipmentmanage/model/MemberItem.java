package com.jth.memberequipmentmanage.model;

import com.jth.memberequipmentmanage.entity.Member;
import com.jth.memberequipmentmanage.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;


@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MemberItem {
    @ApiModelProperty(notes = "사원 시퀀스")
    private Long id;
    @ApiModelProperty(notes = "사원 이름과 직급")
    private String fullName;
    @ApiModelProperty(notes = "사원 근속 여부")
    private Boolean isEnable;
    @ApiModelProperty(notes = "사원 입사일")
    private LocalDateTime dateJoin;

    @ApiModelProperty(notes = "사원 퇴사일")
    private LocalDateTime dateLeave;

    private MemberItem(MemberItemBuilder builder) {
        this.id = builder.id;
        this.fullName = builder.fullName;
        this.isEnable = builder.isEnable;
        this.dateJoin = builder.dateJoin;
        this.dateLeave = builder.dateLeave;
    }

    public static class MemberItemBuilder implements CommonModelBuilder<MemberItem> {
        private final Long id;
        private final String fullName;
        private final Boolean isEnable;
        private final LocalDateTime dateJoin;
        private final LocalDateTime dateLeave;

        public MemberItemBuilder(Member member) {
            this.id = member.getId();
            this.fullName = member.getName() + member.getRank();
            this.isEnable = member.getIsEnable();
            this.dateJoin = member.getDateJoin();
            this.dateLeave = member.getDateLeave();
        }

        @Override
        public MemberItem build() {
            return new MemberItem(this);
        }
    }
}
