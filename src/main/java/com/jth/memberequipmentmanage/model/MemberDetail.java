package com.jth.memberequipmentmanage.model;

import com.jth.memberequipmentmanage.entity.Member;
import com.jth.memberequipmentmanage.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MemberDetail {
    @ApiModelProperty(notes = "사원 시퀀스")
    private Long id;
    @ApiModelProperty(notes = "사원 이름")
    private String name;
    @ApiModelProperty(notes = "사원 직급")
    private String rank;
    @ApiModelProperty(notes = "사원 생년월일")
    private LocalDate birthday;
    @ApiModelProperty(notes = "사원 전화번호")
    private String phone;
    @ApiModelProperty(notes = "사원 성별")
    private String gender;
    @ApiModelProperty(notes = "사원 근속 여부")
    private String isEnable;
    @ApiModelProperty(notes = "사원 입사일")
    private LocalDateTime dateJoin;
    @ApiModelProperty(notes = "사원 퇴사일")
    private LocalDateTime dateLeave;

    private MemberDetail(MemberDetailBuilder builder) {
        this.id = builder.id;
        this.name = builder.name;
        this.rank = builder.rank;
        this.birthday = builder.birthday;
        this.phone = builder.phone;
        this.gender = builder.gender;
        this.isEnable = builder.isEnable;
        this.dateJoin = builder.dateJoin;
        this.dateLeave = builder.dateLeave;
    }

    public static class MemberDetailBuilder implements CommonModelBuilder<MemberDetail> {
        private final Long id;
        private final String name;
        private final String rank;
        private final LocalDate birthday;
        private final String phone;
        private final String gender;
        private final String isEnable;
        private final LocalDateTime dateJoin;
        private final LocalDateTime dateLeave;

        public MemberDetailBuilder(Member member) {
            this.id = member.getId();
            this.name = member.getName();
            this.rank = member.getRank();
            this.birthday = member.getBirthday();
            this.phone = member.getPhone();
            this.gender = member.getGender().getName();
            this.isEnable = member.getIsEnable() ? "예" : "아니오";
            this.dateJoin = member.getDateJoin();
            this.dateLeave = member.getDateLeave();
        }

        @Override
        public MemberDetail build() {
            return new MemberDetail(this);
        }
    }
}
