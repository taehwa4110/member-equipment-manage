package com.jth.memberequipmentmanage.model;

import com.jth.memberequipmentmanage.entity.Equipment;
import com.jth.memberequipmentmanage.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class EquipmentItem {
    @ApiModelProperty(notes = "기자재 시퀀스")
    private Long id;
    @ApiModelProperty(notes = "기자재 분류와 기자재명")
    private String fullName;
    @ApiModelProperty(notes = "기자재 사용 가능 여부")
    private String isUsed;

    @ApiModelProperty(notes = "기자재 구입일")
    private LocalDateTime datePurchase;

    @ApiModelProperty(notes = "기자재 폐기일")
    private LocalDateTime dateDisposal;

    private EquipmentItem(EquipmentItemBuilder builder) {
        this.id = builder.id;
        this.fullName = builder.fullName;
        this.isUsed = builder.isUsed;
        this.datePurchase = builder.datePurchase;
        this.dateDisposal = builder.dateDisposal;
    }


    public static class EquipmentItemBuilder implements CommonModelBuilder<EquipmentItem> {
        private final Long id;
        private final String fullName;
        private final String isUsed;
        private final LocalDateTime datePurchase;
        private final LocalDateTime dateDisposal;

        public EquipmentItemBuilder(Equipment equipment) {
            this.id = equipment.getId();
            this.fullName = "[" + equipment.getClassification().getName() + "]" + equipment.getName();
            this.isUsed = equipment.getIsUsed() ? "예" : "아니오";
            this.datePurchase = equipment.getDatePurchase();
            this.dateDisposal = equipment.getDateDisposal();
        }

        @Override
        public EquipmentItem build() {
            return new EquipmentItem(this);
        }
    }
}
