package com.jth.memberequipmentmanage.controller;

import com.jth.memberequipmentmanage.model.*;
import com.jth.memberequipmentmanage.service.MemberService;
import com.jth.memberequipmentmanage.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "사원 관리 프로그램")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/member")
public class MemberController {
    private final MemberService memberService;

    @ApiOperation(value = "사원 정보 등록하기")
    @PostMapping("/new")
    public CommonResult setMember(@RequestBody @Valid MemberJoinRequest request) {
        memberService.setMember(request);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "사원 정보 하나 가져오기")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "사원 시퀀스", required = true)
    })
    @GetMapping("/{id}")
    public SingleResult<MemberDetail> getMember(@PathVariable long id) {
        return ResponseService.getSingleResult(memberService.getMember(id));
    }

    @ApiOperation(value = "사원 정보 리스트 가져오기")
    @GetMapping("/search")
    public ListResult<MemberItem> getMembers(
            @RequestParam(value = "isEnable", required = false)Boolean isEnable) {
        if (isEnable == null) {
            return ResponseService.getListResult(memberService.getMembers(), true);
        }else {
            return ResponseService.getListResult(memberService.getMembers(isEnable), true);
        }
    }

    @ApiOperation(value = "사원 직급 수정하기")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "사원 시퀀스", required = true)
    })
    @PutMapping("/{id}")
    public CommonResult putMemberRank(@PathVariable long id, @RequestBody @Valid MemberRankUpdateRequest request) {
        memberService.putMemberRank(id, request);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "사원 정보 삭제하기")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "사원 시퀀스", required = true)
    })
    @DeleteMapping("/{id}")
    public CommonResult delMember(@PathVariable long id) {
        memberService.putMemberLeave(id);

        return ResponseService.getSuccessResult();
    }
}
