package com.jth.memberequipmentmanage.controller;

import com.jth.memberequipmentmanage.model.*;
import com.jth.memberequipmentmanage.service.EquipmentService;
import com.jth.memberequipmentmanage.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "기자재 관리 프로그램")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/equipment")
public class EquipmentController {
    private final EquipmentService equipmentService;

    @ApiOperation(value = "기자재 정보 등록하기")
    @PostMapping("/new")
    public CommonResult setEquipment(@RequestBody @Valid EquipmentBuyRequest request) {
        equipmentService.setEquipment(request);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "기자재 정보 하나 가져오기")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "기자재 시퀀스", required = true)
    })
    @GetMapping("/{id}")
    public SingleResult<EquipmentDetail> getEquipment(@PathVariable long id) {
        return ResponseService.getSingleResult(equipmentService.getEquipment(id));
    }

    @ApiOperation(value = "기자재 정보 리스트 가져오기")
    @GetMapping("/search")
    public ListResult<EquipmentItem> getEquipments(
            @RequestParam(value = "isUsed", required = false)Boolean isUsed) {
        if (isUsed == null) {
            return ResponseService.getListResult(equipmentService.getEquipments(), true);
        }else {
            return ResponseService.getListResult(equipmentService.getEquipments(isUsed), true);
        }
    }

    @ApiOperation(value = "기자재 정보 삭제하기")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "기자재 시퀀스", required = true)
    })
    @DeleteMapping("/{id}")
    public CommonResult delEquipment(@PathVariable long id) {
        equipmentService.putEquipmentDisposal(id);

        return ResponseService.getSuccessResult();
    }
}
