package com.jth.memberequipmentmanage.controller;

import com.jth.memberequipmentmanage.entity.Equipment;
import com.jth.memberequipmentmanage.entity.Member;
import com.jth.memberequipmentmanage.model.CommonResult;
import com.jth.memberequipmentmanage.model.ListResult;
import com.jth.memberequipmentmanage.model.UsageHistoryItem;
import com.jth.memberequipmentmanage.model.UsageHistoryRequest;
import com.jth.memberequipmentmanage.service.EquipmentService;
import com.jth.memberequipmentmanage.service.MemberService;
import com.jth.memberequipmentmanage.service.ResponseService;
import com.jth.memberequipmentmanage.service.UsageHistoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;

@Api(tags = "기자재 사용내역 관리 프로그램")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/usage-history")
public class UsageHistoryController {
    private final UsageHistoryService usageHistoryService;
    private final MemberService memberService;
    private final EquipmentService equipmentService;

    @ApiOperation(value = "기자재 사용내역 등록하기")
    @PostMapping("/new")
    public CommonResult setUsageHistory(@RequestBody @Valid UsageHistoryRequest request) {
        Member member = memberService.getMemberData(request.getMemberId());
        Equipment equipment = equipmentService.getEquipmentData(request.getEquipmentId());
        usageHistoryService.setUsageHistory(member, equipment, request.getDateUsage());

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "기간에 해당되는 기자재 사용내역 리스트 가져오기")
    @GetMapping("/search")
    public ListResult<UsageHistoryItem> getUsageHistories(
            @RequestParam(value = "dateStart")@DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate dateStart,
            @RequestParam(value = "deateEnd")@DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate dateEnd) {
        return ResponseService.getListResult(usageHistoryService.getUsageHistories(dateStart, dateEnd), true);
    }
}
